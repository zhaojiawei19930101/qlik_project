module MessagesHelper
  # @desc Strong parameters for message, return a hashtable of the parameters
  # @return Hash 
  # May raise exception ActionController::ParameterMissing if :message is missing
  def message_params
    params.require(:message).permit(:content, :user_name)
  end
end

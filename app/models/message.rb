class Message < ActiveRecord::Base
  
  # Define constants
  USER_NAME_LENGTH_MAX = 255
  #CONTENT_LENGTH_MAX = 
  
  validates :user_name, presence: true, length: {maximum: USER_NAME_LENGTH_MAX}
  validates :content, presence: true
  
  # @desc Determine if the content of the message is a palindrome
  # @return Boolean 
  def palindrome?
    str = self.content.gsub(/\W|_/, "").downcase #only keep word characters and turn it to downcase
    length = str.length
    for i in 0...length/2 do
      if str[i] != str[length-i-1]
        return false
      end
    end
    return true
  end
end

class Error
    
    attr_accessor :code, :field
    
    def initialize(code=nil, field=nil)
        self.code = code
        self.field = field
    end
    
end

json.messages do
  json.array! @messages do |message|
    json.id message.id
    json.content message.content
    json.user_name message.user_name
  end
end
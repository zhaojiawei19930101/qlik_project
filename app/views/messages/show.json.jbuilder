json.message do
  json.id @message.id
  json.content @message.content
  json.user_name @message.user_name
  json.created_at @message.created_at
  json.is_palindrome @message.palindrome?
end
class MessagesController < ApplicationController
  include MessagesHelper
  
  # post /messages
  def create
    @message = Message.new(message_params)
    if @message.save
      respond_to do |format|
        format.json { render :show, status: :created }
      end
    else
      @errors = formattedErrors(@message)
  		respond_to do |format|
  		  format.json { render template: 'shared/errors', status: :bad_request }
  	  end
    end
  end
  
  # get /messages
  def index
    @messages = Message.all.order(created_at: :desc)
    respond_to do |format|
      format.json { render :index, status: :ok }
    end
  end

  #get /messages/:id
  def show
    @message = Message.find(params[:id])
    respond_to do |format|
      format.json { render :show, status: :ok }
    end
  end
  
  #delete /messages/:id
  def destroy
    message = Message.find(params[:id])
    message.destroy
    respond_to do |format|
      format.json { head :ok }
    end
  end
 
end

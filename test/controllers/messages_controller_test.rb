require 'test_helper'

class MessagesControllerTest < ActionController::TestCase
  #--------------------tests for action "create"------------------------------
  test "shoud create a message" do
    assert_difference 'Message.count', 1 do
      post :create, format: :json, message: { content: "This is a message", user_name: "myname" }
    end
    assert_template :show
    assert_response :created
  end
  
  test "should not create an message without parameter :message" do
    assert_no_difference 'Message.count' do
      post :create, format: :json
    end
    assert_template 'shared/errors'
    assert_response :bad_request
    assert json_response["errors"] == [{"code"=> "missing_field", "field"=>"message"}]
  end
  
  test "should not create an invalid message (blank content)" do
    assert_no_difference 'Message.count' do
      post :create, format: :json, message: { content: "  ", user_name: "myname" }
    end
    assert_template 'shared/errors'
    assert_response :bad_request
    assert json_response["errors"] == [{"code"=> "missing_field", "field"=>"Message:content"}]
  end
  
  test "should not create an invalid message (blank user_name)" do
    assert_no_difference 'Message.count' do
      post :create, format: :json, message: { content: "This is a message", user_name: "   " }
    end
    assert_template 'shared/errors'
    assert_response :bad_request
    assert json_response["errors"] == [{"code"=> "missing_field", "field"=>"Message:user_name"}]
  end
  
  test "should not create an invalid message (user_name is too long)" do
    assert_no_difference 'Message.count' do
      post :create, format: :json, message: { content: "This is a message", user_name: "@"*256 }
    end
    assert_template 'shared/errors'
    assert_response :bad_request
    assert json_response["errors"] == [{"code"=> "field_is_too_long", "field"=>"Message:user_name"}]
  end
  
  #------------------------tests for action "index"------------------------
  test "shoud get all messages" do
    get :index, format: :json
    assert_template :index
    assert_response :ok
    assert json_response["messages"] == [
        {
          "id"=>messages(:message_three).id,
          "content"=>messages(:message_three).content,
          "user_name"=>messages(:message_three).user_name
        },
        {
          "id"=>messages(:message_two).id,
          "content"=>messages(:message_two).content,
          "user_name"=>messages(:message_two).user_name
        },
        {
          "id"=>messages(:message_one).id,
          "content"=>messages(:message_one).content,
          "user_name"=>messages(:message_one).user_name
        }
      ]
    
  end
  
  #---------------------tests for action "show"---------------------
  test "shoud get an existing message that is not a palindrome" do
    get :show, format: :json, id: messages(:message_one).id
    assert_template :show
    assert_response :ok
    assert json_response["message"] == {
      "id" => messages(:message_one).id,
      "content" => messages(:message_one).content,
      "user_name" => messages(:message_one).user_name,
      "created_at" => messages(:message_one).created_at.as_json,
      "is_palindrome" => false
    }
    
  end
  
  test "shoud get an existing message that is a palindrome" do
    get :show, format: :json, id: messages(:message_two).id
    assert_template :show
    assert_response :ok
    assert json_response["message"] == {
      "id" => messages(:message_two).id,
      "content" => messages(:message_two).content,
      "user_name" => messages(:message_two).user_name,
      "created_at" => messages(:message_two).created_at.as_json,
      "is_palindrome" => true
    }
    
  end
  
  test "shoud not get an non-existing message" do
    get :show, format: :json, id: 1
    assert_response :not_found
  end
  
  #------------------tests for action "delete"------------------------
  test "shoud delete an existing message" do
    assert_difference 'Message.count', -1 do
      delete :destroy, format: :json, id: messages(:message_two).id
    end
    assert_response :ok
    
  end
  
  test "shoud not delete an non-existing message" do
    assert_no_difference 'Message.count' do
      delete :destroy, format: :json, id: 1
    end
    assert_response :not_found
  end
  

 
end

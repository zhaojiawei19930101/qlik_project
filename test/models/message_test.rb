require 'test_helper'

class MessageTest < ActiveSupport::TestCase
  def setup
    @message = Message.new(content: "Message Content", user_name: "@"*255)
  end
  
  test "should be valid" do
    assert @message.valid?
  end
  
  test 'content should be present' do
    @message.content = "     "
    assert_not @message.valid?
  end
  test 'user_name should be present' do
    @message.user_name = "     "
    assert_not @message.valid?
  end
  
  test 'user_name should not be too long' do
    @message.user_name = "@"*256
    assert_not @message.valid?
  end
end

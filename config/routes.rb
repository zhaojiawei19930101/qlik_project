Rails.application.routes.draw do
    defaults(format: :json) {
      #create a new message
      post "messages", to:"messages#create"
      #list all messages
      get "messages", to: "messages#index"
      #show a specific message
      get "messages/:id", to: "messages#show"
      #delete a specific message
      delete "messages/:id", to: "messages#destroy"
    }
end

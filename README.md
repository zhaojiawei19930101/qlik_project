# Qlik Project


## Architecture 


Below is the diagram of the architecture:

![](https://ws3.sinaimg.cn/large/006tNc79gy1fk4otugmcrj30mz09q0t8.jpg)

#### API Server 
The API server is developed using [Ruby on Rails](http://rubyonrails.org/) and deployed on a cloud platform [Heroku](https://dashboard.heroku.com). The database used is PostgreSQL.

#### Web Client
The front-end client is a single-page web application developed using [ReactJs](https://reactjs.org/). It sends HTTP requests to the API server through AJAX.

## Usage
#### API Server 
Below are the RESTful APIs provided by the API server:
```

Host https://qlik-project.herokuapp.com
GET /messages          #Lists all messages
GET /messages/:id      #Retrieves a specific message
POST /messages         #Submit a message
DELETE /messages/:id   #Delete a specific message
```
#### Web Client
Below are the urls of web pages of the web client:
```

http://localhost/messages    # Lists messages
http://localhost/new_message # Submit a new message
```

## API Documentation
### GET /messages
#### Successful response


```

{  
   "messages":[  
      {  
         "id":1,
         "content":"message1",
         "user_name":"name1"
      },
      {  
         "id":2,
         "content":"message2",
         "user_name":"name2"
      },
      {  
         "id":3,
         "content":"message3",
         "user_name":"name3"
      }
   ]
}
```  
#### statuses and errors

STATUS | ERROR CODE | DESC | ERROR EXAMPLE
-----|:--------:|:--------:|:--------:
ok (200) | N/A | Success | N/A


### GET /messages/:id
#### Successful response


```

{  
   "message":{  
      "id":1,
      "content":"message content",
      "user_name":"myame",
      "created_at":"2017-10-01T22:15:16.718Z",
      "is_palindrome":true
   }
}
```  
#### statuses and errors

STATUS | ERROR CODE | DESC | ERROR EXAMPLE
-----|:--------:|:--------:|:--------:
ok (200) | N/A | Success | N/A
not_found (404) | N/A | cannot find specific message | N/A


### POST /messages
#### Request
```

{  
   "message":{  
      "content":"message content",
      "user_name":"myame"
   }
}
```
required fields

* content
* user_name (cannot longer than 255)

#### Successful response


```

{  
   "message":{  
      "id":1,
      "content":"message content",
      "user_name":"myame",
      "created_at":"2017-10-01T22:15:16.718Z",
      "is_palindrome":true
   }
}
```  
#### statuses and errors

STATUS | ERROR CODE | DESC | ERROR EXAMPLE
-----|:--------:|:--------:|:--------:
created (201) | N/A | Success | N/A
bad_request (400) | missing_field | field is missing | {"code":"missing_field", "field":"message"}
 | missing_field | field is missing | {"code":"missing_field", "field":"Message:user_name"}
 | missing_field | field is missing | {"code":"missing_field", "field":"Message:content"}
 | field_is_too_long | field is too long | {"code":"field_is_too_long", "field":"Message:user_name"}
 
### DELETE /messages/:id
#### statuses and errors

STATUS | ERROR TYPE | DESC | ERROR EXAMPLE
-----|:--------:|:--------:|:--------:
ok (200) | N/A | Success | N/A
not_found (404) | N/A | cannot find specific message | N/A